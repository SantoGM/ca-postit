const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const autoIncrementModelID = require('./counter');

let Schema = mongoose.Schema;

mongoose.set('useCreateIndex', true)

let postitSchema = new Schema({
    code:{
        type: String
    },
    username: {
        type: String,
        unique: false,
        required: [true, 'Es obligatorio un UserName']
    },
    date: {
        type: Date,
        required: [true, 'Es obligatoria la fecha']
    },
    message: {
        type: String,
        required: [true, 'Debe tener una nota']
    }
});

postitSchema.pre('save', function (next) {
    if (!this.isNew) {
      next();
      return;
    }
  
    autoIncrementModelID('activities', this, next);
  });

postitSchema.index({username: 'text', date: 'date'});
postitSchema.plugin(uniqueValidator, { message: '{PATH} debe de ser único' });

module.exports = mongoose.model('Postit', postitSchema);