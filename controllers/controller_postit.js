//DB Entities
const Postit = require("../models/postit");

//**********END CONST DECLARATION***********/

exports.savePostit = (req,res) => {
    let paramMsg = req.body.message
    let paramUsername = req.uncriptedToken.tokenData.username;

    if(paramMsg == null) {
      return res.status(406).json({
        ok: false,
        err: "El mensaje no puede estar vacio"
      });
    }

    let postit = new Postit({
        username: paramUsername,
        date: new Date(),
        message: paramMsg
    })

      postit.save((err, postitDB) => {
        if (err) {
          return res.status(500).json({
            ok: false,
            err
          });
        }
        res.json({
          ok: true,
          mail: postitDB
        });
      });
}

exports.getPostit = (req, res)=>{
  let paramUsername = req.uncriptedToken.tokenData.username;

  Postit.find({username: paramUsername}, (err, postits) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      postits
    });
  })
}

exports.getPostitByDate = (req, res)=>{
  let paramUsername = req.uncriptedToken.tokenData.username;
  let paramDateBefore = req.params.dateBefore;
  let paramDateAfter = req.params.dateAfter;

  let castedBeforeDate = new Date(paramDateBefore)
  let castedAfterDate = new Date(paramDateAfter)

  if (castedBeforeDate == "Invalid Date" || castedAfterDate == "Invalid Date"){
    return res.status(400).json({
      ok: false,
      err: "El formato de las fechas es incorrecto"
    });
  }

  if (castedBeforeDate >= castedAfterDate){
    return res.status(400).json({
      ok: false,
      err: "Las fechas anterior y posterior no son congruentes"
    });
  }

  Postit.find({username: paramUsername, date: { $gte: paramDateBefore, $lte: paramDateAfter }}, (err, postits) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      postits
    });
  })
}


exports.getPostitByCode = (req, res)=>{
  let paramUsername = req.uncriptedToken.tokenData.username;
  let paramCode = req.params.postitCode;

  if (paramCode == null || isNaN(paramCode)){
    return res.status(406).json({
      ok: false,
      err: "Debe enviar un codigo de PostIt"
    });
  }

  Postit.find({username: paramUsername, code: paramCode}, (err, postits) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    if (postits.length > 0) {
      res.json({
        ok: true,
        postits
      });
    } else {
      res.status(404).json({
        ok: true,
        error: "No hay nota con el código indicado"
      });
    }
  })
}

exports.updatePostIt = async (req, res)=>{
  let paramUsername = req.uncriptedToken.tokenData.username;
  let paramCode = req.params.postitCode;
  let paramNewMessage = req.body.newMessage;
  
  if (paramCode == null || isNaN(paramCode)){
    return res.status(406).json({
      ok: false,
      err: "Debe enviar un codigo de PostIt"
    });
  }

  if (paramNewMessage == null){
    return res.status(401).json({
      ok: false,
      err: "EL mensaje no puede estar vacio"
    })
  }

  let update = await Postit.updateOne({username: paramUsername, code: paramCode}, {message: paramNewMessage}, (err, res) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err
      });
    }
  })

  if (update.n == 0) {
    return res.status(400).json({
      ok: true,
      err: "No se encontro la nota a actualizar"
    })
  } else {
    return res.json({
      ok: true,
      msj: "Se actualizo la nota correctamente"
    })
  }
}


exports.deletePostIt = async (req, res) => {
  let paramUsername = req.uncriptedToken.tokenData.username;
  let paramCode = req.params.postitCode;

  if (paramCode == null || isNaN(paramCode)){
    return res.status(406).json({
      ok: false,
      err: "Debe enviar un codigo de PostIt"
    });
  }

  let del = await Postit.deleteOne({username: paramUsername, code: paramCode}).exec()

  if (del.ok !== 1) {
    return res.status(500).json({
      ok: false,
      err: "Ocurrio un error al intentar eliminar la nota"
    })
  } else {    
    if (del.n == 0) {
      return res.status(400).json({
        ok: true,
        err: "No se encontro la nota a eliminar"
      }) 
    } else {
      return res.status(200).json({
        ok: true,
        message: "Postit eliminado exitosamente"
      })
    }
  }
}