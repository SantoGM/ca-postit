//*********BEGIN CONST DECLARATION**********/
//Server const
const express = require("express");
const app = express();

//Middlewares
const { validateToken } = require("../middlewares/validateToken");

//Controller
const controllerPostit = require("../controllers/controller_postit")

//**********END CONST DECLARATION***********/

/**
 * @swagger
 * /api/postit/:
 *  post:
 *    tags:
 *      - postit
 *    description: Si el usuario está logueado, crea un nuevo Postit con el mensaje que viene por body.
 *    parameters:
 *      - in: header
 *        name: token
 *        required: true
 *      - in: body
 *        name: message
 *        description: el mensaje a guardar
 *        schema:
 *          type: object
 *          required:
 *            - message
 *          properties:
 *            message:
 *              type: string
 *    produces:
 *      - applicatioon/json
 *    responses:
 *      200:
 *        description: Devuelve JSon con el mensaje creado
 *      400:
 *        description: No se pudo conectar a la BD
 *      401:
 *        description: Token inválido
 *      500:
 *        description: Error al guardar
 */
app.post("/", validateToken, controllerPostit.savePostit)

/**
 * @swagger
 * /api/postit/:
 *  get:
 *    tags:
 *      - postit
 *    parameters:
 *      - in: header
 *        name: token
 *        required: true
 *    description: Token de validación.
 *    produces:
 *      - applicatioon/json
 *    responses:
 *      200:
 *        description: Devuelve JSon con todas las notas del usuario
 *      400:
 *        description: Error al buscar las notas
 *      401:
 *        description: Token inválido
 */
app.get("/", validateToken, controllerPostit.getPostit)

/**
 * @swagger
 * /api/postit/getPostitByDate/{dateBefore}/{dateAfter}:
 *  get:
 *    tags:
 *      - postit
 *    parameters:
 *      - in: header
 *        name: token
 *        required: true
 *      - in: path
 *        name: dateBefore
 *        required: true
 *      - in: path
 *        name: dateAfter
 *        required: true
 *    description: Token de validación.
 *    produces:
 *      - applicatioon/json
 *    responses:
 *      200:
 *        description: Devuelve JSon con las notas del usuario entre las fechas especificadas
 *      400:
 *        description: Error al buscar las notas
 *      401:
 *        description: Token inválido
 */
app.get("/getPostitByDate/:dateBefore/:dateAfter", validateToken, controllerPostit.getPostitByDate)


/**
 * @swagger
 * /api/postit/getPostitByCode/{postitCode}:
 *  get:
 *    tags:
 *      - postit
 *    parameters:
 *      - in: header
 *        name: token
 *        required: true
 *      - in: path
 *        name: postitCode
 *        required: true
 *    description: Token de validación.
 *    produces:
 *      - applicatioon/json
 *    responses:
 *      200:
 *        description: Devuelve JSon con la nota del código especificado
 *      400:
 *        description: Error al buscar las notas
 *      401:
 *        description: Token inválido
 *      404:
 *        description: No hay nota con ese código
 */
app.get("/getPostitByCode/:postitCode", validateToken, controllerPostit.getPostitByCode)

/**
 * @swagger
 * /api/postit/updatePostIt/{postitCode}:
 *  put:
 *    tags:
 *      - postit
 *    parameters:
 *      - in: header
 *        name: token
 *        required: true
 *      - in: path
 *        name: postitCode
 *        required: true
 *      - in: body
 *        name: newMessage
 *        type: string
 *        required: true
 *    responses:
 *      200:
 *        description: Da el OK del update
 *      400:
 *        description: Error al buscar las notas
 *      401:
 *        description: Token inválido
 */
app.put("/updatePostIt/:postitCode", validateToken, controllerPostit.updatePostIt)


/**
 * @swagger
 * /api/postit/deletePostIt/{postitCode}:
 *  delete:
 *    tags:
 *      - postit
 *    parameters:
 *      - in: header
 *        name: token
 *        required: true
 *      - in: path
 *        name: postitCode
 *        required: true
 *    responses:
 *      200:
 *        description: Da el OK del update
 *      400:
 *        description: Error al buscar las notas
 *      401:
 *        description: Token inválido
 */
app.delete("/deletePostIt/:postitCode", validateToken, controllerPostit.deletePostIt)

module.exports = app