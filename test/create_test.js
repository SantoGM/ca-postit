let chai = require("chai");
let chaihttp = require("chai-http");
var jwt = require("jsonwebtoken");
//incluir el app.js para que mocha pueda ejecutar las pruebas
let app = require("../app");
let should = chai.should();

const testToken = jwt.sign({ username: "testUser", ultimoLogin:new Date() }, process.env.CLAVEJWT,{
    expiresIn: 60 * 60 * 24 // expira en 24 horas
});

chai.use(chaihttp);
chai.should();

var postitCode

describe("Postit creation test: ", () => {

    it("should fail due to empty message", (done) => {
        chai.request(app)
            .post("/api/postit/")
            .set('token', testToken)
            .send({
                message: null
            })
            .then((res) => {
                res.should.be.json;

                chai.assert.equal(res.status, 406, "No devuelve status code 406");
                done();             
            }).catch((err) =>{
                done(new Error(err.message))
            }) ;
    });

    it("should create a note", (done) => {
        chai.request(app)
            .post("/api/postit/")
            .set('token', testToken)
            .send({
                message:"Mensaje de prueba"
            })
            .then((res) => {
                res.should.be.json;

                chai.assert.equal(res.status, 200, "No devuelve status code 200");
                postitCode = res.body.mail.code
                done();             
            }).catch((err) =>{
                done(new Error(err.message))
            }) ;
    });
});

describe("Postit retrieve test: ", () => {
    it("should fail due to not sending numeric code", (done) => {
        chai.request(app)
            .get("/api/postit/getPostitByCode/x")
            .set('token', testToken)
            .then((res) => {
                res.should.be.json
                chai.assert.equal(res.status, 406, "No devuelve status code 406");
                chai.assert.equal(res.ok, false, "El resultado es OK")
                done();             
            }).catch((err) =>{
                done(new Error(err.message))
            });
    });

    it("should retrieve the note by code", (done) => {
        chai.request(app)
            .get("/api/postit/getPostitByCode/" + postitCode)
            .set('token', testToken)
            .then((res) => {
                res.should.be.json
                chai.assert.equal(res.status, 200, "No devuelve status code 200");
                chai.assert.equal(res.ok, true, "El resultado no es OK")
                done();             
            }).catch((err) =>{
                done(new Error(err.message))
            }) ;
    });

    it("should retrieve all the notes for the logged user", (done) => {
        chai.request(app)
            .get("/api/postit/")
            .set('token', testToken)
            .then((res) => {
                res.should.be.json
                chai.assert.equal(res.status, 200, "No devuelve status code 200");
                chai.assert.equal(res.ok, true, "El resultado no es OK")
                done();             
            }).catch((err) =>{
                done(new Error(err.message))
            }) ;
    });

    it("should fail beacause of date format", (done) => {
        chai.request(app)
            .get("/api/postit/getPostitByDate/datebefore/123456")
            .set('token', testToken)
            .then((res) => {
                res.should.be.json
                chai.assert.equal(res.status, 400, "No devuelve status code 400");
                chai.assert.equal(res.ok, false, "El resultado es OK")
                done();             
            }).catch((err) =>{
                done(new Error(err.message))
            }) ;
    });

    it("should fail beacause dates are inverted", (done) => {
        chai.request(app)
            .get("/api/postit/getPostitByDate/2020-01-01/2001-01-01")
            .set('token', testToken)
            .then((res) => {
                res.should.be.json
                chai.assert.equal(res.status, 400, "No devuelve status code 400");
                chai.assert.equal(res.ok, false, "El resultado es OK")
                done();             
            }).catch((err) =>{
                done(new Error(err.message))
            }) ;
    });

    it("should succeed, bringing any postit between selected dates", (done) => {
        chai.request(app)
            .get("/api/postit/getPostitByDate/2000-01-01/2100-01-01")
            .set('token', testToken)
            .then((res) => {
                res.should.be.json
                chai.assert.equal(res.status, 200, "No devuelve status code 200");
                chai.assert.equal(res.ok, true, "El resultado no es OK")
                done();             
            }).catch((err) =>{
                done(new Error(err.message))
            }) ;
    });
});

describe("Postit update test: ", () => {
    
    it("should fail due to non numeric code", (done) => {
        chai.request(app)
            .put("/api/postit/updatePostit/x")
            .set('token', testToken)
            .send({
                newMessage:"Mensaje editado"
            })
            .then((res) => {
                res.should.be.json;

                chai.assert.equal(res.status, 406, "No devuelve status code 406");
                done();             
            }).catch((err) =>{
                done(new Error(err.message))
            }) ;
    });

    it("should fail due to empty message", (done) => {
        chai.request(app)
            .put("/api/postit/updatePostit/999")
            .set('token', testToken)
            .send({
                newMessage: null
            })
            .then((res) => {
                res.should.be.json;

                chai.assert.equal(res.status, 401, "No devuelve status code 401");
                done();             
            }).catch((err) =>{
                done(new Error(err.message))
            }) ;
    });

    it("should fail for unexisting note", (done) => {
        chai.request(app)
            .put("/api/postit/updatePostit/999")
            .set('token', testToken)
            .send({
                newMessage: "Mensaje editado"
            })
            .then((res) => {
                res.should.be.json;
                chai.assert.equal(res.status, 400, "No devuelve status code 400");
                done();             
            }).catch((err) =>{
                done(new Error(err.message))
            }) ;
    });

    it("should update the note succesfuly", (done) => {
        chai.request(app)
            .put("/api/postit/updatePostit/" + postitCode)
            .set('token', testToken)
            .send({
                newMessage:"Mensaje editado"
            })
            .then((res) => {
                res.should.be.json;

                chai.assert.equal(res.status, 200, "No devuelve status code 200");
                done();             
            }).catch((err) =>{
                done(new Error(err.message))
            }) ;
    });
});

describe("Postit delete test: ", () => {
    
    it("should fail due to non numeric code", (done) => {
        chai.request(app)
            .delete("/api/postit/deletePostIt/x")
            .set('token', testToken)
            .then((res) => {
                res.should.be.json;
                chai.assert.equal(res.status, 406, "No devuelve status code 406");
                done();             
            }).catch((err) =>{
                done(new Error(err.message))
            }) ;
    });

    
    it("should fail for unexisting note", (done) => {
        chai.request(app)
            .delete("/api/postit/deletePostIt/999")
            .set('token', testToken)
            .then((res) => {
                res.should.be.json;
                chai.assert.equal(res.status, 400, "No devuelve status code 400");
                done();             
            }).catch((err) =>{
                done(new Error(err.message))
            }) ;
    });

    it("should delete the note", (done) => {
        chai.request(app)
            .delete("/api/postit/deletePostIt/" + postitCode)
            .set('token', testToken)
            .then((res) => {
                res.should.be.json;
                chai.assert.equal(res.status, 200, "No devuelve status code 200");
                done();             
            }).catch((err) =>{
                done(new Error(err.message))
            }) ;
    });
});