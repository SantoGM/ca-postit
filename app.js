require("./config/config")

const express = require('express')
const app = express()
const connectionHelper = require('./helper/connectionHelper')

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use("/api/postit/", require("./routes/route_postit"));

/******INICIO CONFIGURACIÓN SWAGGER ******/

//paquetes npm para que swagger funcione
const swaggerUi = require('swagger-ui-express')
const swaggerJsdoc = require('swagger-jsdoc');

//la especificacion que figurara en la doc automatica
const swaggerDefinition = {
  info: {
    title: "CA | REST-IT",
    version: "1.0.0",
    description: "API de notas post-it"
  },
  host: "localhost:" + process.env.PORT,
  basePath: "/"
};

//donde va a "mirar" swagger para exponer la doc
const options = {
  swaggerDefinition,
  apis: ["./routes/*.js"],
  customCss: '.swagger-ui .topbar { display: none }'
};

//finalizacion de las configuraciones
const swaggerSpec = swaggerJsdoc(options);
app.get('/swagger.json', function(req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(swaggerSpec);
});

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

/********FIN CONFIGURACIÓN SWAGGER *******/

//Toma el puerto del archivo config/config.js
app.listen(process.env.PORT, function() {
  connectionHelper.connectDB()
    console.log(
      "Servidor de Postit iniciado en el puerto " + process.env.PORT + "!"
    );
  });

//DEBO EXPORTAR app PARA LOS TEST EN MOCHA
module.exports = app;