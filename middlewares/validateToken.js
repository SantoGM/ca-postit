exports.validateToken = async function(req, res, next) {
  var jwt = require("jsonwebtoken");
  var token = req.headers.token;
  if (!token) {
    return res
      .status(401)
      .json({ error: "Es necesario el token de autenticación" });
  }

  var resultado = await jwt.verify(token, process.env.CLAVEJWT, function(
    err,
    tokenData
  ) {
    if (err) {
      return res.status(401).json({ error: 'Token invalido, ingresar nuevamente a la aplicacion' });
    } else {
      req.uncriptedToken = { tokenData };
    }
    next();
  });
};
